let http  = require("http");

let port = 4000;

let server = http.createServer((req, res) => {

// get
	if (req.url == "/" && req.method == "GET" ){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to Booking System!");
	} else if (req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to your profile! ");
	} else if (req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Here’s our courses available.");
	}


// post
	if(req.url == "/addcourse" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain" });
		res.end("Add a course to our resources.");
	}

// put 
	if(req.url == "/updatecourse" && req.method == "PUT"){
		res.writeHead(200, {"Content-Type" : "text/plain" });
		res.end("Update a course to our resources.");
	}

// delete
	if(req.url == "/archivecourses" && req.method == "DELETE"){
		res.writeHead(200, {"Content-Type" : "text/plain" });
		res.end("Archive courses to our resources.");
	}

});

server.listen(port);
console.log(`Server is running at local host: ${port}.`);